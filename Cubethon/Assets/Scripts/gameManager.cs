﻿using UnityEngine;
﻿using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour {

    bool gameHasEnded = false;

    public float delay = 1.5f;

    public GameObject completeLevelUI;

    public void completeLevel() {
      completeLevelUI.SetActive(true);
    }

    public void EndGame () {
      if (gameHasEnded == false);
        gameHasEnded = true;
        Debug.Log("Game over!");
        Invoke("Restart", delay);
      }

    void Restart () {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
